package piramidetriangulo;

class PiramidesTriangulos {

public void piramides() {
    
     System.out.println("(a)\n"); 
      
      for(int i = 0; i < 10; i++){
          for (int j = 0; j < i + 1; j++){
              System.out.print("*");
          }
              System.out.println("");
      } 
      
      System.out.println("");
      
      System.out.println("(b)\n");
      
      for(int h = 0; h < 10; h++){
          for(int k = h; k < 10; k++){
              System.out.print("*");
          }
              System.out.println("");
      }
      
      System.out.println("");
      
      System.out.println("(c)\n");
      
      for(int c = 0; c < 10; c++){
          for(int b = 0; b < c+1; b++){
              System.out.print(" ");
          }
          for(int b2 = c; b2 < 10; b2++){
              System.out.print("*");
          }
              System.out.println("");
      }
      
      System.out.println("");
      
      System.out.println("(d)\n");
      
      for(int m = 0; m < 10; m++){
          for(int n = m; n < 10; n++){
              System.out.print(" ");
          }
          
          for(int n2 = 0; n2 < m+1; n2++){
              System.out.print("*");
          }
              System.out.println("");
      }
        
    }
    
}
