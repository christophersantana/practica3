package minimo3;

public class Min3 {

public double minimo(double a, double b, double c) {
    return Math.min(a, Math.min(b, c));
    
    }         
}
