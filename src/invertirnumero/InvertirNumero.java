package invertirnumero;

public class InvertirNumero {

public static void main(String[] args) {
    
    System.out.println("Este programa lee un numero y lo muestra de forma invertida");
    
    System.out.println("Digite un numero");
        
int numero = new java.util.Scanner(System.in).nextInt();

int r;

while(numero>0){
    r=numero%10;
    numero=numero/10;
    
    System.out.print(r);            
}
System.out.println("");       
    
    }
    
}
